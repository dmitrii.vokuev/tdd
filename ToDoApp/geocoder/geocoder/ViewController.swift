//
//  ViewController.swift
//  geocoder
//
//  Created by dmitrii on 07.08.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString("Berlin") { (placemarks, error) in
            let placemark = placemarks?.first
            let latitude = placemark?.location?.coordinate.latitude
            let longitude = placemark?.location?.coordinate.longitude
            
            print(latitude, longitude)
        }
        
    }
    
    


}

