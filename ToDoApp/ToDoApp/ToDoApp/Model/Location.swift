//
//  Location.swift
//  ToDoApp
//
//  Created by dmitrii on 26.07.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import Foundation
import CoreLocation


struct Location {
    let name: String
    let coordinate: CLLocationCoordinate2D?
    var dict: [String: Any] {
        var dict: [String : Any] = [:]
        dict["name"] = name
        if let coordinate = coordinate {
            dict["longitude"] = coordinate.longitude
            dict["latitude"] = coordinate.latitude
        }
        return dict
    }
    
    init(name: String, coordinate: CLLocationCoordinate2D? = nil) {
        self.name = name
        self.coordinate = coordinate
    }
}


extension Location: Equatable {
    static func ==(lhs: Location, rhs: Location) -> Bool {
        guard rhs.coordinate?.latitude == lhs.coordinate?.latitude &&
            rhs.coordinate?.longitude == lhs.coordinate?.longitude &&
        lhs.name == rhs.name else { return false }
        return true
    }
}

extension Location {
    typealias PlistDictionary = [String : Any]
    init?(dict: PlistDictionary) {
        self.name = dict["name"] as! String
        if
            let latitude = dict["latitude"] as? Double,
            let longitude = dict["longitude"] as? Double{
            self.coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        } else {
            self.coordinate = nil
        }
    }

}
