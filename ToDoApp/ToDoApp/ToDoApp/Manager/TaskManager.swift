//
//  TaskManager.swift
//  ToDoApp
//
//  Created by dmitrii on 30.07.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import UIKit

class TaskManager {
    
    private var taskArray = [Task]()
    private var doneTaskArray = [Task]()
    
    var taskCount: Int {
        return taskArray.count
    }
    
    var taskURL: URL {
        let fileURLs = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        guard let documentURL = fileURLs.first else {
            fatalError()
        }
        return documentURL.appendingPathComponent("tasks.plist")
    }
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(save), name: UIApplication.willResignActiveNotification, object: nil)
        
        if let data = try? Data(contentsOf: taskURL) {
            let dictionaries = try? PropertyListSerialization.propertyList(from: data,
                                                                           options: [],
            format: nil) as! [[String : Any]]
            
            for dict in dictionaries! {
                if let task = Task(dict: dict) {
                    taskArray.append(task)
                }
            }
        }
    }
    
    deinit {
        save()
    }
    
    @objc
    func save() {
        let taskDicitionaries = self.taskArray.map { $0.dict }
        guard taskDicitionaries.count > 0 else {
            try? FileManager.default.removeItem(at: taskURL)
            return
        }
        
        let plistData = try? PropertyListSerialization.data(fromPropertyList: taskDicitionaries,
                                                            format: .xml,
                                                            options: PropertyListSerialization.WriteOptions(0))
        
        try? plistData?.write(to: taskURL, options: .atomic)
    }
    
    
    var doneTasksCount: Int {
        return doneTaskArray.count
    }
    
    func add(task: Task) {
        if !taskArray.contains(task) {
            taskArray.append(task)
        }
    }
    
    func tasks(at index: Int) -> Task {
        return taskArray[index]
    }
    
    func checkTask(at index: Int) {
      var task = taskArray.remove(at: index)
        task.isDone.toggle()
        doneTaskArray.append(task)
    }
    
    func unCheckTask(at index: Int) {
        var task = doneTaskArray.remove(at: index)
        task.isDone.toggle()
        taskArray.append(task)
    }
    
    func doneTasks(at index: Int) -> Task {
        return doneTaskArray[index]
    }
    
    func removeAll() {
        taskArray.removeAll()
        doneTaskArray.removeAll()
    }
}
