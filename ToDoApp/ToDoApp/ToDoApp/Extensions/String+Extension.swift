//
//  String+Extension.swift
//  ToDoApp
//
//  Created by dmitrii on 10.08.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import Foundation

extension String {
    var percentEncoded: String {
        let allowedCharacters = CharacterSet(charactersIn: "~!@#$%^&*()-+=[]\\}{.,/?><").inverted
        guard let encodeString = self.addingPercentEncoding(withAllowedCharacters: allowedCharacters) else {
            fatalError()
        }
        return encodeString
    }
}
