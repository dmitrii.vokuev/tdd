//
//  TaskTest.swift
//  ToDoAppTests
//
//  Created by dmitrii on 26.07.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import XCTest
import MapKit
@testable import ToDoApp

class TaskTest: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    
    func testInitTaskWithTitle() {
        let task = Task(title: "FOO")
        
        XCTAssertNotNil(task)
    }
    
    func testInitTaskWithTitleAndDescription() {
        let task = Task(title: "FOO", description: "Bar")
        XCTAssertNotNil(task)
    }
    
    func testWhenGivenTitleSetsTitle() {
        let task = Task(title: "Foo")
        
        XCTAssertEqual(task.title, "Foo")
    }
    
    func testWhenGivenDescriptionSetsTitle() {
        let task = Task(title: "Foo", description: "Bar")
        
        XCTAssertEqual(task.description, "Bar")
        // the same check but different method
        XCTAssertTrue(task.description == "Bar")
    }
    
    func testTaskInitsWithDate() {
        let task = Task(title: "Foo")
        XCTAssertNotNil(task.date)
    }
    
   
    
    func testWhenGivenLocationSetsLocation() {
        let location = Location(name: "Foo")
        let task = Task(title: "Bar", description: "Baz", location: location)
        XCTAssertEqual(location, task.location)
    }
    
    func testCanBeCreatedFromPlistDictionary() {
        let location = Location(name: "Baz")
        let date = Date(timeIntervalSince1970: 10)
        let task = Task(title: "Foo", description: "Bar", location: location, date: date)
        let locationDictinary: [String: Any] = ["name" : "Baz"]

        let dictionary: [String : Any] = ["title": "Foo",
                                          "description": "Bar",
                                          "date": date,
                                          "location" : locationDictinary]

        let createdTask = Task(dict: dictionary)
        
        XCTAssertEqual(task, createdTask)
    }
    
    func testCanBeSerializedIntoDictionary() {
        let location = Location(name: "Baz")
        let date = Date(timeIntervalSince1970: 10)
        let task = Task(title: "Foo", description: "Bar", location: location, date: date)
        
        let generatedTask = Task(dict: task.dict)
        
        XCTAssertEqual(task, generatedTask )
    }
}

