//
//  ToDoAppTests.swift
//  ToDoAppTests
//
//  Created by dmitrii on 26.07.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import XCTest
@testable import ToDoApp

class ToDoAppTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testInitialViewControllerIsTaskListViewController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let navigantionController = storyboard.instantiateInitialViewController() as! UINavigationController
        let rootViewController = navigantionController.viewControllers.first as! TaskListViewController
        XCTAssertTrue(rootViewController is TaskListViewController)
    }
    
    
}
