//
//  TaskManagerTests.swift
//  ToDoAppTests
//
//  Created by dmitrii on 30.07.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import XCTest
@testable import ToDoApp

class TaskManagerTests: XCTestCase {
    
    var sut: TaskManager!
    
    override func setUp() {
        super.setUp()
        sut = TaskManager()
    }
    
    
    override func tearDown() {
        sut.removeAll()
        sut = nil
        super.tearDown()
    }
    
    
    func testInitTaskManagerWithZeroTasks() {
        XCTAssertEqual(sut.taskCount, 0)
    }
    
    func testInitTaskManagerWithZeroDoneTasks() {
        XCTAssertEqual(sut.doneTasksCount, 0)
    }
    
    func testAddTaskIncrementTaskCount() {
        let task = Task(title: "Foo")
        sut.add(task: task)
        
        XCTAssertEqual(sut.taskCount, 1)
    }
    
    func testTaskAtIndexIsAddedTask() {
        let task = Task(title: "Foo")
        sut.add(task: task)
        
        let returnedTask = sut.tasks(at: 0)
        XCTAssertEqual(task.title, returnedTask.title)
    }
    
    func testTaskPropertyIsDoneEqualFalse() {
        let task = Task(title: "Foo")
        XCTAssertEqual(task.isDone, false)
    }
    
    func testTaskPropertyIsDoneEqualTrue() {
        let task = Task(title: "Foo")
        sut.add(task: task)
        sut.checkTask(at: 0)
        print("check the task \(sut.doneTasksCount)")
        XCTAssertEqual(sut.doneTasks(at: 0).isDone, true)
        
    }
    
    func  testCheckTaskAtIndexChangesCounts() {
        let task = Task(title: "Foo")
        sut.add(task: task)

        sut.checkTask(at: 0)
        XCTAssertEqual(sut.taskCount, 0)
        XCTAssertEqual(sut.doneTasksCount, 1)
    }
    
    func testCheckedTaskRemovedFromTasks() {
        let firstTask = Task(title: "Foo")
        let secondTask = Task(title: "Bar")
        
        
        sut.add(task: firstTask)
        sut.add(task: secondTask)
        
        sut.checkTask(at: 0)
        
        XCTAssertEqual(sut.tasks(at: 0), secondTask)
    }
    
    
    func testDoneTaskAtReturnsCheckedTask() {
        let task = Task(title: "Foo")
        sut.add(task: task)
        sut.checkTask(at: 0)
        let returnedTask = sut.doneTasks(at: 0)
        XCTAssertEqual(returnedTask, task)
    }
    
    func testRemoveAllResultsCountsBeZero() {
        sut.add(task: Task(title: "Foo"))
        sut.add(task: Task(title: "Bar"))
        sut.checkTask(at: 0)
        
        sut.removeAll()
        
        XCTAssertTrue(sut.doneTasksCount == 0)
        XCTAssertTrue(sut.taskCount == 0)
    }
    
    func testAddingSameObjectDoesntIncrementCount() {
        sut.add(task: Task(title: "Foo"))
        sut.add(task: Task(title: "Foo"))
        
        XCTAssertTrue(sut.taskCount == 1)
    }
    
    func testWhenTaskManagerRecreatedSavedTasksShouldBeEqual() {
        var taskManager: TaskManager! = TaskManager()
        let task = Task(title: "Foo")
        let task1 = Task(title: "Bar")
        
        taskManager.add(task: task)
        taskManager.add(task: task1)
        
        NotificationCenter.default.post(name: UIApplication.willResignActiveNotification, object: nil)
        
        taskManager = nil
        
        taskManager = TaskManager()

        XCTAssertEqual(taskManager.taskCount, 2)
        XCTAssertEqual(taskManager.tasks(at: 0), task)
        XCTAssertEqual(taskManager.tasks(at: 1), task1)

    }
    
}
