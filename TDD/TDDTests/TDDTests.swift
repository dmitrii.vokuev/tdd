//
//  TDDTests.swift
//  TDDTests
//
//  Created by dmitrii on 25.07.19.
//  Copyright © 2019 dmitrii. All rights reserved.
//

import XCTest
@testable import TDD

class TDDTests: XCTestCase {

    let sut = ViewController()
    
    override func setUp() {
        super.setUp()
        
    }

    override func tearDown() {
        
      //  sut = nil
        super.tearDown()
    }

    
    func testLowestVolumeShouldBeZero() {
        sut.setVolume(value: -100)
        let volume = sut.volume
        XCTAssert(volume == 0, "Lowest value should be equal 0.")
    }

    func testHighesttVolumeShouldBe100() {
        sut.setVolume(value: 200)
        let volume = sut.volume
        XCTAssert(volume == 100, "Highest value should be equal 0.")
    }
    
    
    func testCharsInStringsAreTheSame() {
        // given
        let string1 = "qwerty"
        let string2 = "ytrewq"
        
        // when
        let bool = sut.charactersCompare(stringOne: string1, stringTwo: string2)
        
        // then
        XCTAssert(bool, "characters should be the same in two strings")
    }
    
    func testCharsInStringsAreTheDifferent() {
        // given
        let string1 = "qwerty"
        let string2 = "ytrew"
        
        // when
        let bool = sut.charactersCompare(stringOne: string1, stringTwo: string2)
        
        // then
        XCTAssert(!bool, "characters should be the same in two strings")
    }

}
